import nacl.secret
import nacl.utils
import nacl.encoding
import nacl.signing
import argparse
import qrcode


def print_new_topic_keys():
    signing_key = nacl.signing.SigningKey.generate()
    signing_key_hex = signing_key.encode(encoder=nacl.encoding.HexEncoder).decode('utf8')
    verify_key_hex = signing_key.verify_key.encode(encoder=nacl.encoding.HexEncoder).decode('utf8')
    psk = nacl.utils.random(nacl.secret.SecretBox.KEY_SIZE).hex()
    print("Topic's private key: %s\nTopic's public key: %s\nTopic's pre shared key: %s" % (signing_key_hex,
                                                                                                 verify_key_hex, psk))
    qr = qrcode.QRCode()
    qr.add_data(verify_key_hex + psk)
    qr.print_ascii()
    print("Keep those keys safe!")


def main():
    parser = argparse.ArgumentParser(description="MQTT E2E keys generator")
    parser.add_argument("command", choices=["new"], help="new - create topic keys")
    args = parser.parse_args()
    if args.command == "new":
        print_new_topic_keys()


if __name__ == '__main__':
    main()
