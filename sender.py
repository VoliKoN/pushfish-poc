import paho.mqtt.client as mqtt_api
import nacl.signing
import nacl.encoding
import nacl.secret
import argparse

SERVER = "test-api.push.fish"
PORT = 1883


class MqttTransceiver:
    def __init__(self):
        self.client = mqtt_api.Client()
        self.client.connect(SERVER, PORT, 60)

    def publish(self, message, topic):
        self.client.publish(topic, message)

    def start_loop(self):
        self.client.loop_start()

    def close(self):
        self.client.loop_stop()
        self.client.disconnect()


def deserialize(sterilized_key):
    result = bytes.fromhex(sterilized_key)
    return result


def send_message(privkey, psk, msg):
    box = nacl.secret.SecretBox(deserialize(psk))
    encrypted_message = box.encrypt(msg.encode("utf-8"))
    signing_key = nacl.signing.SigningKey(deserialize(privkey))
    signed_encrypted_message = signing_key.sign(encrypted_message)
    mqtt = MqttTransceiver()
    mqtt.publish(signed_encrypted_message,
                 signing_key.verify_key.encode(encoder=nacl.encoding.HexEncoder).decode("utf-8"))


def main():
    parser = argparse.ArgumentParser(description="MQTT E2E message sender")
    parser.add_argument("privkey", help="topic's private key")
    parser.add_argument("psk", help="topic's psk")
    parser.add_argument("msg", help="message to send")
    args = parser.parse_args()
    send_message(args.privkey, args.psk, args.msg)


if __name__ == '__main__':
    main()
