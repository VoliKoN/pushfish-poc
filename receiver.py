import paho.mqtt.client as mqtt_api
import nacl.signing
import nacl.encoding
import nacl.secret
import argparse

SERVER = "test-api.push.fish"
PORT = 1883


class MqttTransceiver:
    def __init__(self, topics, callback):
        self.topics = topics
        self.callback = callback
        self.client = mqtt_api.Client()
        self.client.on_message = self.message_callback
        self.client.on_connect = self.connect_callback
        self.client.connect(SERVER, PORT, 60)

    def message_callback(self, client, userdata, msg):
        self.callback(msg.topic, msg.payload)

    def connect_callback(self, client, userdata, flags, rc):
        for topic in self.topics:
            self.client.subscribe(topic)

    def publish(self, message, topic):
        self.client.publish(topic, message)

    def start_loop(self):
        self.client.loop_start()

    def close(self):
        self.client.loop_stop()
        self.client.disconnect()


class Client:
    def __init__(self, pubkey, psk):
        self.pubkey = pubkey
        self.verify_key = nacl.signing.VerifyKey(pubkey, encoder=nacl.encoding.HexEncoder)
        self.topic = self.pubkey
        self.psk = psk
        self.mqtt = MqttTransceiver([self.topic], self.received_message)
        self.mqtt.start_loop()

    def received_message(self, topic, message):
        assert topic == self.topic == self.pubkey
        encrypted_message = self.verify_key.verify(message)
        box = nacl.secret.SecretBox(self.psk, encoder=nacl.encoding.HexEncoder)
        plaintext_message = box.decrypt(encrypted_message)
        print(plaintext_message.decode("utf-8"))

    def close(self):
        self.mqtt.close()


def main():
    parser = argparse.ArgumentParser(description="MQTT E2E message receiver")
    parser.add_argument("pubkey", help="topic's pubkey")
    parser.add_argument("psk", help="topic's psk")
    args = parser.parse_args()
    client = Client(args.pubkey, args.psk)
    while True:
        pass


if __name__ == '__main__':
    main()
