# PushFish-PoC

PoC for end-to-end encrypted communication protocol over MQTT  

### Pros:
* You don't need to run a server, only an MQTT broker.
* There's only one sender but infinite number of recipients.
* Messages are only encrypted and signed once.
* The only requirements for communicating using this protocol 
are NaCl and MQTT client, So you can probably 
use: Android, IOS, JS, ESP8266 and any device running python.
* If you don't have the PSK you can't read the messages.
* If you don't have the privkey you can't send messages.
* The MQTT broker only knows the pubkey, and can't read 
or send messages.
* The MQTT broker can verify that messages are sent by the proper sender,
and can block frequent impostors.
* I never noticed any delays using all of those scripts on my pretty capable pc
(Haswell I5 running latest Arch Linux).
### Cons:
* You need to transfer the pubkey and PSK by other secure 
means (preferably by other one-to-one method in PushFish).
* To let someone read from your topic you need to give them 
the pubkey, PSK and MQTT server address (or a QR code containing them all).
* Dumb clients can't use this method easily, as they need to 
run code for encrypting and communicating with MQTT, See my 
suggestion for this problem below.

## Notes:
* This is only a prototype and will have to be integrated to 
a Python library with proper error handling.
* I only learned about NaCl and end-to-end encryption recently, 
so all of this could easily be just plain garbage.
 
## Solution for dumb clients
My solution for this is developing a Flask HTTP endpoint that will create topics and
send messages to them.  
* The privkey will be saved on the server running the endpoint.
* The communication will be only encrypted server-to-end, but it can be 
secured using HTTPS.
* This is great for services like Sonarr or IFTTT that you want to 
notify you about not so important stuff.
* I think the [PushFish-API](https://gitlab.com/PushFish/PushFish-API) can 
be easily converted for this job.

---
## Installation
Use local python 3 or venv.   
Run:
```bash
pip3 install -r requirements.txt
```
The scripts currently using our public broker on `test-api.push.fish`, but you can change this.
## Using
### generator.py
This script generates 3 keys:
  1. Topic's private key.
  2. Topic's public key.
  3. Topic's PSK (pre shared key).

The script also prints a QR code to terminal 
containing the pubkey and the PSK concatenated.

#### Usage
```bash
python3 generator.py new
```
Output (the QR will look better on a proper terminal):
```
Topic's private key: e3676cc3b3db962e413cbc9ad64281dfc4dbac4193efc62ba1653cedaf98a986
Topic's public key: 01e3ecc7af6b4660fdacce2df4c3ea19eb4bec601e0cc24365b866f72c9d6267
Topic's pre shared key: af7981d42a1dced054a985352c22a09ef3223e8bfebe53ce301de9ce6496df5c
                                                         
                                                         
    █▀▀▀▀▀█  █ ▀▄ ▀▀▄██▀█▄▄▀▀▄ ▄ ▄█▀ ▀ ▀█▀▄▄█ █▀▀▀▀▀█    
    █ ███ █ █▄▄▄▄▀█▄▄▄ ▄ ▀ ▀█▄▄▀▀██▀▀█ ▀▄  █▀ █ ███ █    
    █ ▀▀▀ █ ██  ▄▄█▄█▄▄▄ ▀█▀▀▀██▄▄▀▄▀▄▄ ▄█▄   █ ▀▀▀ █    
    ▀▀▀▀▀▀▀ █▄█ █▄▀ █ ▀ ▀ █ ▀ █▄█▄█ █ ▀▄▀ ▀ ▀ ▀▀▀▀▀▀▀    
    █ ▀▀██▀ ▄█▄▄▀██▀█ ▀▀▀▀▀███▀   ██ ▀▀ ▀▀▀▀▀ ██▀█▀▄     
    ▄▀█▄▄ ▀█▀▀█ ▄█ ▄▀ ▀▀▀▄  ▀▄▀▀▄▀▄ ██▀█▀█▀▀▀▀▄█▄ ▀ ▀    
     █  ▄▄▀█▀▄▀▄██▄▀▀▄▀▄▄ █▀▀ ▀▀▄▀▄█▄▀▀ ▀▀ ▄▀█▄▄ ▀▀█▀    
    █▀ ▄█▄▀ ▄ ▄▀█ ▀▀█▄ ▄█▀█▄▀▀▀█ █   ▀█▀██▀█▄ ▄▀▄▀  █    
       ▀  ▀█▀█ ▀ ▄██▄▀ ▀▀▀  █▄▀▄█▄ ▀█▀█▀ ▄ ▄▀█▄▀ █▀ ▀    
    ▄▀██▀▀▀▄██▀▄▀█ █ ▄██   ▄ ▄█ █ █▀█▀ █ ▄▀▀ ▄▄ ▄  ██    
    ████▀ ▀██  ██ █ ▄█▀▀▀▀▀▄█▄█▀ ▄▄█ ▀▀▄▀▄▀▀▀▄██ █▀ ▀    
    ▄  ██▀▀▀█▄▄▀  ▄▀▄▀ ▀ ██▀▀▀█▄▄▄▄█▄▀▀▄▀▄▀▄█▀▀▀██ ▀█    
    ▀▀█ █ ▀ █▄▀▀█ ▀▀ █▀▄▀██ ▀ █ ▄ ▄▄▄█▀ █ ▀██ ▀ █ ▀█▀    
    ▀██▄▀▀▀█▀▄▀▄▄█ █▀ █ ▀▄█▀█▀▀▄▄▀▄ ▄▄▀▄ ▀▀ ▀▀▀█▀  ▄▀    
     ██▀▄█▀▀█ █▀▀ █▀  ▀▀ ██▄▀█ ▄  ▄█▄▄  ▀▀▀█▀▀▀▄▀▀▀▄▀    
    ▀██ ▀▀▀  ███      █▀ ▄▄▄ ▀▄  ▀▄▄ ▄▀▀██▀ ▀▄▀▀▀▄  █    
    ▄▀█▄▀▄▀▄▄▄ ▄  ▄██▄▀▄▀█▄▄▄█▄▀ ▀█▄ ▄ ▄▀ ▀█▀▄▀▀▀ ▀▄▀    
    ▀▀▄▄  ▀▄▀▄██  ▀▀▄▄█▀▀█▄██▄▄█▄██ ▄   ▀▀▀ ▀▄▀  ▄  █    
    █▄ ▀ ▀▀▀ ▄ █▄▄ ▄██ ▄▀▄ ▀▄▀▄ ▄▄██ ▀ ▀▀ ▀▀ ▄▀▀▀▄▀ ▀    
     █▄▄ ▀▀▀▀▄ ▄███  ██ ▀▄▄▀▀▀▄▀▄ ▄ █▀▀▄▀█▀▀▄▄▀█   ██    
    ▀▀▀   ▀▀█▄▄  ▀▄ ▀█▀▄▀▄█▀▀▀██▄ ▄▀▄▄▀ ▀█ ▀█▀▀▀█▀▀ ▀    
    █▀▀▀▀▀█ ▄  █   ▀▀▄▀▄ ██ ▀ █ ▄▀▄ ▄▄▀▄▀▄▀ █ ▀ ██ ██    
    █ ███ █ █▄▀▀▀▀█▄▄█ ▄▄▀██▀▀▀█▄ ▄█ █  █▀ ▀█▀█▀█▄█▀█    
    █ ▀▀▀ █ ▀▀█ █ ▄█ █▄▄█▀▄▄█▀ ▄ █    █▀▀█▀█ ▀██▄   ▄    
    ▀▀▀▀▀▀▀ ▀  ▀  ▀ ▀▀  ▀   ▀▀  ▀  ▀▀▀▀▀▀▀   ▀   ▀▀▀▀    
                                                         
                                                         
Keep those keys safe!

```

---
### sender.py
This is the script used to send messages.  
The script needs the Topic's privkey and PSK (the pubkey 
is derived from the privkey).

#### What the script does
 1. Encrypt the message using the PSK.
 2. Sign the encrypted message using the privkey.
 3. Publish the encrypted and signed message using the pubkey as the MQTT topic.
#### Usage
```bash
python3 sender.py privkey psk message
```
Example:
```bash
python3 sender.py e3676cc3b3db962e413cbc9ad64281dfc4dbac4193efc62ba1653cedaf98a986 af7981d42a1dced054a985352c22a09ef3223e8bfebe53ce301de9ce6496df5c hello
```

---
### receiver.py
This script listen to messages on a topic and needs 
the pubkey and PSK of the topic.

#### What the script does
 1. Subscribe to MQTT using the pubkey as topic.
 2. Wait for a message to arrive.
 3. Continue to listen to messages.
 
When message arrive:
 1. Verify the message is coming from the true sender using the pubkey.
 2. Decrypt the message using the PSK.
 3. Print to the user the decrypted message

#### Usage
```bash
python3 receiver.py pubkey psk
```
Example:
```bash
python3 receiver.py 01e3ecc7af6b4660fdacce2df4c3ea19eb4bec601e0cc24365b866f72c9d6267 af7981d42a1dced054a985352c22a09ef3223e8bfebe53ce301de9ce6496df5c
```
